package com.softwareone.webhooklistener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebhookListenerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebhookListenerApplication.class, args);
	}

}
