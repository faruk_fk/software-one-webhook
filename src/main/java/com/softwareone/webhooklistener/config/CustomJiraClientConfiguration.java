package com.softwareone.webhooklistener.config;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.net.URI;

@Configuration
public class CustomJiraClientConfiguration {
    private final JiraConfigModel jiraConfigModel;

    public CustomJiraClientConfiguration(JiraConfigModel jiraConfigModel) {
        this.jiraConfigModel = jiraConfigModel;
    }

    @Bean(name = "customJiraRestClient")
    public JiraRestClient getJiraRestClient() {
        return new AsynchronousJiraRestClientFactory()
                .createWithBasicHttpAuthentication(
                        URI.create(jiraConfigModel.getBaseUrl()),
                        jiraConfigModel.getUserMail(),
                        jiraConfigModel.getApiToken()
                );
    }
}
