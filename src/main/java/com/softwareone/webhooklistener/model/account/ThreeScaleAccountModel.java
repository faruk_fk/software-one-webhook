package com.softwareone.webhooklistener.model.account;

import com.softwareone.webhooklistener.model.user.ThreeScaleUsersResponseModel;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ThreeScaleAccountModel {
    private Long id;
    private Date created_at;
    private Date updated_at;
    private String state;
    private String org_name;
    private ThreeScaleUsersResponseModel users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public ThreeScaleUsersResponseModel getUsers() {
        return users;
    }

    public void setUsers(ThreeScaleUsersResponseModel users) {
        this.users = users;
    }
}
