package com.softwareone.webhooklistener.model.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "extra_fields")
@XmlAccessorType(XmlAccessType.FIELD)
public class EventObjectApplicationExtraFieldsModel {
    @XmlElement(name = "estimatedCalls-prod")
    private String estimatedCalls;
    @XmlElement(name = "usage")
    private String usage;

    public String getEstimatedCalls() {
        return estimatedCalls;
    }

    public void setEstimatedCalls(String estimatedCalls) {
        this.estimatedCalls = estimatedCalls;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }
}
