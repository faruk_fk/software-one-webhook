package com.softwareone.webhooklistener.model.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "keys")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class EventObjectApplicationKeyModel {
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
