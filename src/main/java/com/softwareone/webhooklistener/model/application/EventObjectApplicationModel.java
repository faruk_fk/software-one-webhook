package com.softwareone.webhooklistener.model.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "application")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class EventObjectApplicationModel {
    private Long id;
    private String name;
    private String description;
    private Date created_at;
    private Date updated_at;
    private String state;
    private Long user_account_id;
    private String first_traffic_at;
    private String first_daily_traffic_at;
    private Long service_id;
    private String application_id;
    private String redirect_url;
    private List<EventObjectApplicationKeyModel> keys;
    private EventObjectApplicationPlanModel plan;
    private EventObjectApplicationOidcConfigurationModel oidc_configuration;
    private EventObjectApplicationExtraFieldsModel extra_fields;

    public EventObjectApplicationExtraFieldsModel getExtra_fields() {
        return extra_fields;
    }

    public void setExtra_fields(EventObjectApplicationExtraFieldsModel extra_fields) {
        this.extra_fields = extra_fields;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getUser_account_id() {
        return user_account_id;
    }

    public void setUser_account_id(Long user_account_id) {
        this.user_account_id = user_account_id;
    }

    public String getFirst_traffic_at() {
        return first_traffic_at;
    }

    public void setFirst_traffic_at(String first_traffic_at) {
        this.first_traffic_at = first_traffic_at;
    }

    public String getFirst_daily_traffic_at() {
        return first_daily_traffic_at;
    }

    public void setFirst_daily_traffic_at(String first_daily_traffic_at) {
        this.first_daily_traffic_at = first_daily_traffic_at;
    }

    public Long getService_id() {
        return service_id;
    }

    public void setService_id(Long service_id) {
        this.service_id = service_id;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    public List<EventObjectApplicationKeyModel> getKeys() {
        return keys;
    }

    public void setKeys(List<EventObjectApplicationKeyModel> keys) {
        this.keys = keys;
    }

    public EventObjectApplicationPlanModel getPlan() {
        return plan;
    }

    public void setPlan(EventObjectApplicationPlanModel plan) {
        this.plan = plan;
    }

    public EventObjectApplicationOidcConfigurationModel getOidc_configuration() {
        return oidc_configuration;
    }

    public void setOidc_configuration(EventObjectApplicationOidcConfigurationModel oidc_configuration) {
        this.oidc_configuration = oidc_configuration;
    }
}
