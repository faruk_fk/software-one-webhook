package com.softwareone.webhooklistener.model.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "oidc_configuration")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class EventObjectApplicationOidcConfigurationModel {
    private Long id;
    private boolean standard_flow_enabled;
    private boolean implicit_flow_enabled;
    private boolean service_accounts_enabled;
    private boolean direct_access_grants_enabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isStandard_flow_enabled() {
        return standard_flow_enabled;
    }

    public void setStandard_flow_enabled(boolean standard_flow_enabled) {
        this.standard_flow_enabled = standard_flow_enabled;
    }

    public boolean isImplicit_flow_enabled() {
        return implicit_flow_enabled;
    }

    public void setImplicit_flow_enabled(boolean implicit_flow_enabled) {
        this.implicit_flow_enabled = implicit_flow_enabled;
    }

    public boolean isService_accounts_enabled() {
        return service_accounts_enabled;
    }

    public void setService_accounts_enabled(boolean service_accounts_enabled) {
        this.service_accounts_enabled = service_accounts_enabled;
    }

    public boolean isDirect_access_grants_enabled() {
        return direct_access_grants_enabled;
    }

    public void setDirect_access_grants_enabled(boolean direct_access_grants_enabled) {
        this.direct_access_grants_enabled = direct_access_grants_enabled;
    }
}
