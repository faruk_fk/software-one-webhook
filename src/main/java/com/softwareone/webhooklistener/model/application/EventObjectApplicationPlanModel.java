package com.softwareone.webhooklistener.model.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "plan")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class EventObjectApplicationPlanModel {
    private Long id;
    private String name;
    private String type;
    private String state;
    private boolean approval_required;
    private String setup_fee;
    private String cost_per_month;
    private String trial_period_days;
    private Long cancellation_period;
    private Long service_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isApproval_required() {
        return approval_required;
    }

    public void setApproval_required(boolean approval_required) {
        this.approval_required = approval_required;
    }

    public String getSetup_fee() {
        return setup_fee;
    }

    public void setSetup_fee(String setup_fee) {
        this.setup_fee = setup_fee;
    }

    public String getCost_per_month() {
        return cost_per_month;
    }

    public void setCost_per_month(String cost_per_month) {
        this.cost_per_month = cost_per_month;
    }

    public String getTrial_period_days() {
        return trial_period_days;
    }

    public void setTrial_period_days(String trial_period_days) {
        this.trial_period_days = trial_period_days;
    }

    public Long getCancellation_period() {
        return cancellation_period;
    }

    public void setCancellation_period(Long cancellation_period) {
        this.cancellation_period = cancellation_period;
    }

    public Long getService_id() {
        return service_id;
    }

    public void setService_id(Long service_id) {
        this.service_id = service_id;
    }
}
