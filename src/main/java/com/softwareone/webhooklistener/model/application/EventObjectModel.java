package com.softwareone.webhooklistener.model.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "object")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class EventObjectModel {
    private EventObjectApplicationModel application;

    public EventObjectApplicationModel getApplication() {
        return application;
    }

    public void setApplication(EventObjectApplicationModel application) {
        this.application = application;
    }
}
