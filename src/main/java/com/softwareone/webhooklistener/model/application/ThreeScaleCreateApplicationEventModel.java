package com.softwareone.webhooklistener.model.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "event")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ThreeScaleCreateApplicationEventModel {
    private String action;
    private String type;
    private EventObjectModel object;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EventObjectModel getObject() {
        return object;
    }

    public void setObject(EventObjectModel object) {
        this.object = object;
    }
}
