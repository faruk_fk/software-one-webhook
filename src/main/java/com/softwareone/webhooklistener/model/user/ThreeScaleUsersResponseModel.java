package com.softwareone.webhooklistener.model.user;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)
public class ThreeScaleUsersResponseModel {
    @XmlElement(name = "user")
    private List<ThreeScaleUserModel> user;

    public List<ThreeScaleUserModel> getUser() {
        return user;
    }

    public void setUser(List<ThreeScaleUserModel> user) {
        this.user = user;
    }
}
