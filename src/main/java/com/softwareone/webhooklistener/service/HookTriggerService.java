package com.softwareone.webhooklistener.service;

public interface HookTriggerService {
    void triggerHook(String hookRequestModel) throws Exception;
}
