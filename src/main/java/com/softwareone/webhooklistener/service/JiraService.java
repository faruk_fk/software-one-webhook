package com.softwareone.webhooklistener.service;

public interface JiraService {
    String createIssue(String summary,String details);
}
