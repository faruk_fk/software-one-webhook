package com.softwareone.webhooklistener.service;

import com.softwareone.webhooklistener.model.account.ThreeScaleAccountModel;
import com.softwareone.webhooklistener.model.service.ThreeScaleServiceResponseModel;

public interface ThreeScaleService {
    ThreeScaleAccountModel getAccountById(Long id);
    ThreeScaleServiceResponseModel getServiceById(Long id);
}
