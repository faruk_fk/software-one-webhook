package com.softwareone.webhooklistener.service.impl;

import com.softwareone.webhooklistener.model.account.ThreeScaleAccountModel;
import com.softwareone.webhooklistener.model.application.ThreeScaleCreateApplicationEventModel;
import com.softwareone.webhooklistener.model.service.ThreeScaleServiceResponseModel;
import com.softwareone.webhooklistener.model.user.ThreeScaleUserModel;
import com.softwareone.webhooklistener.model.user.ThreeScaleUsersResponseModel;
import com.softwareone.webhooklistener.service.HookTriggerService;
import com.softwareone.webhooklistener.service.JiraService;
import com.softwareone.webhooklistener.service.ThreeScaleService;
import com.softwareone.webhooklistener.util.ThreeScaleResponseParserUtil;
import com.softwareone.webhooklistener.util.XmlStringToObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class HookTriggerServiceImpl implements HookTriggerService {
    private final XmlStringToObjectMapper xmlStringToObjectMapper;
    private final ThreeScaleService threeScaleService;
    private final JiraService jiraService;
    private final ThreeScaleResponseParserUtil threeScaleResponseParserUtil;

    public HookTriggerServiceImpl(XmlStringToObjectMapper xmlStringToObjectMapper, ThreeScaleService threeScaleService, JiraService jiraService, ThreeScaleResponseParserUtil threeScaleResponseParserUtil) {
        this.xmlStringToObjectMapper = xmlStringToObjectMapper;
        this.threeScaleService = threeScaleService;
        this.jiraService = jiraService;
        this.threeScaleResponseParserUtil = threeScaleResponseParserUtil;
    }

    @Override
    public void triggerHook(String hookRequestModel) throws Exception {
        log.info("triggering hook with request : {}",hookRequestModel);
        log.info("converting xml request to dto");
        ThreeScaleCreateApplicationEventModel applicationEventModel = xmlStringToObjectMapper.mapXml(hookRequestModel,ThreeScaleCreateApplicationEventModel.class);
        if (applicationEventModel.getObject().getApplication().getPlan().isApproval_required()) {
            log.info("getting user of application event trigger account, account id : {}",applicationEventModel.getObject().getApplication().getUser_account_id());
            ThreeScaleAccountModel accountModel = this.threeScaleService.getAccountById(applicationEventModel.getObject().getApplication().getUser_account_id());
            ThreeScaleServiceResponseModel serviceModel = this.threeScaleService.getServiceById(applicationEventModel.getObject().getApplication().getService_id());
            log.info("xml request converted to application event dto");
            String description = this.threeScaleResponseParserUtil.parserAndGenerateDescription(applicationEventModel,accountModel,serviceModel.getName());
            log.info("Jira task description : {}",description);
            String summary = applicationEventModel.getObject().getApplication().getName() + " Uygulamasının Oluşturulması";
            log.info("summary : {}",summary);
            String taskResponse = this.jiraService.createIssue(summary, description);
            log.info("task response : {}",taskResponse);
        } else {
            log.info("plan approval required is false");
        }
    }
}
