package com.softwareone.webhooklistener.service.impl;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.softwareone.webhooklistener.config.JiraConfigModel;
import com.softwareone.webhooklistener.service.JiraService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class JiraServiceImpl implements JiraService {
    private final JiraRestClient jiraRestClient;
    private final JiraConfigModel jiraConfigModel;

    @Autowired
    public JiraServiceImpl(@Qualifier("customJiraRestClient") JiraRestClient jiraRestClient, JiraConfigModel jiraConfigModel) {
        this.jiraRestClient = jiraRestClient;
        this.jiraConfigModel = jiraConfigModel;
    }

    @Override
    public String createIssue(String summary, String details) {
        return this.jiraRestClient.getIssueClient()
                .createIssue(new IssueInputBuilder(this.jiraConfigModel.getProjectKey(), jiraConfigModel.getIssueTypeId(), summary).setDescription(details).build())
                .claim()
                .getKey();
    }
}
