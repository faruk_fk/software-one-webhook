package com.softwareone.webhooklistener.service.impl;

import com.google.gson.Gson;
import com.softwareone.webhooklistener.config.ThreeScaleConfigModel;
import com.softwareone.webhooklistener.model.account.ThreeScaleAccountModel;
import com.softwareone.webhooklistener.model.service.ThreeScaleServiceResponseModel;
import com.softwareone.webhooklistener.service.ThreeScaleService;
import com.softwareone.webhooklistener.util.Constants;
import com.softwareone.webhooklistener.util.XmlStringToObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Log4j2
@Service
public class ThreeScaleServiceImpl implements ThreeScaleService {
    private final ThreeScaleConfigModel threeScaleConfigModel;
    private final XmlStringToObjectMapper xmlStringToObjectMapper;
    private final RestTemplate restTemplate;

    public ThreeScaleServiceImpl(ThreeScaleConfigModel threeScaleConfigModel, XmlStringToObjectMapper xmlStringToObjectMapper, @Qualifier("customRestTemplate") RestTemplate restTemplate) {
        this.threeScaleConfigModel = threeScaleConfigModel;
        this.xmlStringToObjectMapper = xmlStringToObjectMapper;
        this.restTemplate = restTemplate;
    }

    @SneakyThrows
    @Override
    public ThreeScaleAccountModel getAccountById(Long id) {
        log.info("getting account by id : {}",id);
        String url = String.format("%s/" + Constants.THREE_SCALE_ACCOUNT_BY_ID_API_ENDPOINT,this.threeScaleConfigModel.getBaseUrl(),id,this.threeScaleConfigModel.getAccessToken());
        log.info("request url : {}",url);
        String apiRes = restTemplate.getForObject(url, String.class);
        ThreeScaleAccountModel accountModelRes = this.xmlStringToObjectMapper.mapXml(apiRes, ThreeScaleAccountModel.class);
        log.info("account Model Response Obj : {}",new Gson().toJson(accountModelRes));
        return accountModelRes;
    }

    @SneakyThrows
    @Override
    public ThreeScaleServiceResponseModel getServiceById(Long id) {
        log.info("getting service by id : {}",id);
        String url = String.format("%s/" + Constants.THREE_SCALE_SERVICE_BY_ID_API_ENDPOINT,this.threeScaleConfigModel.getBaseUrl(),id,this.threeScaleConfigModel.getAccessToken());
        log.info("request url : {}",url);
        String apiRes = restTemplate.getForObject(url, String.class);
        ThreeScaleServiceResponseModel serviceResponseModel = this.xmlStringToObjectMapper.mapXml(apiRes, ThreeScaleServiceResponseModel.class);
        log.info("service Response Obj : {}",new Gson().toJson(serviceResponseModel));
        return serviceResponseModel;
    }
}
