package com.softwareone.webhooklistener.util;

public class Constants {
    public static final String THREE_SCALE_ACCOUNT_BY_ID_API_ENDPOINT = "accounts/%s.xml?access_token=%s";
    public static final String THREE_SCALE_SERVICE_BY_ID_API_ENDPOINT = "services/%s.xml?access_token=%s";
}
