package com.softwareone.webhooklistener.util;

import com.softwareone.webhooklistener.model.account.ThreeScaleAccountModel;
import com.softwareone.webhooklistener.model.application.ThreeScaleCreateApplicationEventModel;
import com.softwareone.webhooklistener.model.user.ThreeScaleUserModel;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ThreeScaleResponseParserUtil {

    public String parserAndGenerateDescription(ThreeScaleCreateApplicationEventModel applicationEventModel, ThreeScaleAccountModel accountModel,String serviceName) throws Exception {
        StringBuilder msg = new StringBuilder();
        msg.append("Name : ")
                .append(applicationEventModel.getObject().getApplication().getName())
                .append("\n")
                .append("Description : ")
                .append(applicationEventModel.getObject().getApplication().getDescription())
                .append("\n")
                .append("Estimated Calls : ")
                .append(applicationEventModel.getObject().getApplication().getExtra_fields().getEstimatedCalls())
                .append("\n")
                .append("Usage : ")
                .append(applicationEventModel.getObject().getApplication().getExtra_fields().getUsage())
                .append("Plan Name : ")
                .append(applicationEventModel.getObject().getApplication().getPlan().getName())
                .append("\n");
        msg.append("Servis Adı : ")
                .append(serviceName)
                .append("\n");
        msg.append("Account Organization Name : ")
                .append(accountModel.getOrg_name())
                .append("\n")
                .append("Account Id : ")
                .append(accountModel.getId())
                .append("\n");
        if (!accountModel.getUsers().getUser().isEmpty()) {
            ThreeScaleUserModel adminUser = accountModel.getUsers().getUser()
                    .stream()
                    .filter(i -> i.getRole().equalsIgnoreCase("admin"))
                    .findFirst()
                    .orElseThrow(() -> new Exception("Can not found admin user in user list"));
            msg.append("Admin user name : ")
                    .append(adminUser.getUsername())
                    .append("\n")
                    .append("Admin user mail : ")
                    .append(adminUser.getEmail());
        }
        return msg.toString();
    }
}
