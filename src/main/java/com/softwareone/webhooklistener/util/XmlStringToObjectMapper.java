package com.softwareone.webhooklistener.util;

import org.springframework.stereotype.Component;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

@Component
public class XmlStringToObjectMapper {

    public <T> T mapXml(String xml,Class<T> targetClass) throws Exception {
        JAXBContext jaxbContext;
        try{
            jaxbContext = JAXBContext.newInstance(targetClass);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (T) jaxbUnmarshaller.unmarshal(new StringReader(xml));
        }
        catch (JAXBException e) {
            throw new Exception(e);
        }
    }

}
