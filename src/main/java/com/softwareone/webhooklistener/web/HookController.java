package com.softwareone.webhooklistener.web;

import com.softwareone.webhooklistener.service.HookTriggerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/web-hook")
@Log4j2
@CrossOrigin(origins = "*")
public class HookController {
    private final HookTriggerService hookTriggerService;

    public HookController(HookTriggerService hookTriggerService) {
        this.hookTriggerService = hookTriggerService;
    }

    @PostMapping(consumes = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<String> webHookListener(@RequestBody String obj) throws Exception {
        log.info("web hook listener triggered, content : {}",obj);
        this.hookTriggerService.triggerHook(obj);
        return ResponseEntity.ok("ok");
    }
}
